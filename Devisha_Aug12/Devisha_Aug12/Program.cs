﻿using System;
using System.Collections;

namespace Devisha_Aug12
{
    
       
    class Program
    {
        private void btnArray_Click(Object sender, EventArgs e)
        {
            ArrayList list = new ArrayList();
            list.Add(123);
            list.Add("Test");
            list.Add(2.39);

            IEnumerator iEnum = list.GetEnumerator();

            string msg = "";
            while (iEnum.MoveNext())
            {
                msg += iEnum.Current.ToString() + "\n";
            }
            Console.WriteLine(msg, "GetEnumerator() on ArrayList");
        }
        static void Main(string[] args)
        {
            Console.WriteLine("Hello World!");
        }
    }
}
