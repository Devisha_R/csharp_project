﻿using System;
using System.Collections;

namespace Devisha_Queue_Aug15
{
    class Program
    {
        static void Main(string[] args)
        {
           Queue q = new Queue();
            q.Enqueue("Hi");
            q.Enqueue("Hello");

            Console.WriteLine("Queue : ");
            foreach (object o in q)
            {
                Console.WriteLine(o);
            }
            Console.WriteLine(q.Count);
            object obj1 = q.Dequeue();
            Console.WriteLine("Queue after removing element : ");
            foreach (object o in q)
            {
                Console.WriteLine(o);
            }
        }
    }
}
