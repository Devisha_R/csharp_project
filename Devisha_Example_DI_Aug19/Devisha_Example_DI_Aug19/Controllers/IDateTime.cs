﻿namespace Devisha_Example_DI_Aug19.Controllers
{
    public interface IDateTime
    {
        object Now { get; set; }
    }
}