﻿using Devisha_Example_DI_Aug19.Models;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Logging;
using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Threading.Tasks;

namespace Devisha_Example_DI_Aug19.Controllers
{
    public class HomeController : Controller
    {
        private readonly IDateTime _dateTime;

        private readonly ILogger<HomeController> _logger;
        public HomeController(IDateTime dateTime)
        {
            _dateTime = dateTime;

        }
        //public HomeController(ILogger<HomeController> logger)
        //{
        //    _logger = logger;
        //}

        public IActionResult Index()
        {
            var serverTime = _dateTime.Now;
            if (serverTime.Hour <12)
            {
                ViewData["Message"] = "it is ning here - Good Morning";
            } 
            else if (serverTime.Hour < 17)
            {
                ViewData["Message"]
            }
            return View();
        }

        public IActionResult Privacy()
        {
            return View();
        }

        [ResponseCache(Duration = 0, Location = ResponseCacheLocation.None, NoStore = true)]
        public IActionResult Error()
        {
            return View(new ErrorViewModel { RequestId = Activity.Current?.Id ?? HttpContext.TraceIdentifier });
        }
    }
}
