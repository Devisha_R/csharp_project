﻿using System;

namespace Devisha_Procedure_Cal_Aug9
{
    class Program
    {
        static void cal(int num1,int num2)
        {
            Console.WriteLine("Addition is : " + (num1 + num2));
            Console.WriteLine("Sub is : " +( num1 - num2));
            Console.WriteLine("Multiplication is : " + num1*num2);
            Console.WriteLine("Division is : " + num1/num2);

        }
        static void Main(string[] args)
        {
            int num1, num2;
            Console.WriteLine("Enter two number : ");
            num1 = Convert.ToInt32(Console.ReadLine());
            num2 = Convert.ToInt32(Console.ReadLine());
            cal(num1, num2);
        }
    }
}
