﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.Rendering;
using Microsoft.EntityFrameworkCore;
using Devisha_DbFirst_Aug22.Models;

namespace Devisha_DbFirst_Aug22.Controllers
{
    public class EmployeeTable1Controller : Controller
    {
        private readonly CompanyDBContext _context;

        public EmployeeTable1Controller(CompanyDBContext context)
        {
            _context = context;
        }

        // GET: EmployeeTable1
        public async Task<IActionResult> Index()
        {
            return View(await _context.EmployeeTable1.ToListAsync());
        }

        // GET: EmployeeTable1/Details/5
        public async Task<IActionResult> Details(int? id)
        {
            if (id == null)
            {
                return NotFound();
            }

            var employeeTable1 = await _context.EmployeeTable1
                .FirstOrDefaultAsync(m => m.Empid == id);
            if (employeeTable1 == null)
            {
                return NotFound();
            }

            return View(employeeTable1);
        }

        // GET: EmployeeTable1/Create
        public IActionResult Create()
        {
            return View();
        }

        // POST: EmployeeTable1/Create
        // To protect from overposting attacks, enable the specific properties you want to bind to, for 
        // more details, see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> Create([Bind("Empid,Empname")] EmployeeTable1 employeeTable1)
        {
            if (ModelState.IsValid)
            {
                _context.Add(employeeTable1);
                await _context.SaveChangesAsync();
                return RedirectToAction(nameof(Index));
            }
            return View(employeeTable1);
        }

        // GET: EmployeeTable1/Edit/5
        public async Task<IActionResult> Edit(int? id)
        {
            if (id == null)
            {
                return NotFound();
            }

            var employeeTable1 = await _context.EmployeeTable1.FindAsync(id);
            if (employeeTable1 == null)
            {
                return NotFound();
            }
            return View(employeeTable1);
        }

        // POST: EmployeeTable1/Edit/5
        // To protect from overposting attacks, enable the specific properties you want to bind to, for 
        // more details, see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> Edit(int id, [Bind("Empid,Empname")] EmployeeTable1 employeeTable1)
        {
            if (id != employeeTable1.Empid)
            {
                return NotFound();
            }

            if (ModelState.IsValid)
            {
                try
                {
                    _context.Update(employeeTable1);
                    await _context.SaveChangesAsync();
                }
                catch (DbUpdateConcurrencyException)
                {
                    if (!EmployeeTable1Exists(employeeTable1.Empid))
                    {
                        return NotFound();
                    }
                    else
                    {
                        throw;
                    }
                }
                return RedirectToAction(nameof(Index));
            }
            return View(employeeTable1);
        }

        // GET: EmployeeTable1/Delete/5
        public async Task<IActionResult> Delete(int? id)
        {
            if (id == null)
            {
                return NotFound();
            }

            var employeeTable1 = await _context.EmployeeTable1
                .FirstOrDefaultAsync(m => m.Empid == id);
            if (employeeTable1 == null)
            {
                return NotFound();
            }

            return View(employeeTable1);
        }

        // POST: EmployeeTable1/Delete/5
        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> DeleteConfirmed(int id)
        {
            var employeeTable1 = await _context.EmployeeTable1.FindAsync(id);
            _context.EmployeeTable1.Remove(employeeTable1);
            await _context.SaveChangesAsync();
            return RedirectToAction(nameof(Index));
        }

        private bool EmployeeTable1Exists(int id)
        {
            return _context.EmployeeTable1.Any(e => e.Empid == id);
        }
    }
}
