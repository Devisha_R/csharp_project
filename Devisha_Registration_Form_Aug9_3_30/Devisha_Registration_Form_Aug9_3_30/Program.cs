﻿using System;

namespace Devisha_Registration_Form_Aug9_3_30
{
    class Program
    {
        static void Main(string[] args)
        {
            Console.WriteLine("Registration Form");
            //Declaring Variables : 
            
            String ID, name,college;
            DateTime dob;
            char gender;
            double height;

            //Getting Input From User
            Console.WriteLine("Enter Id : ");
            ID = Console.ReadLine();
            Console.WriteLine("Enter Name : ");
            name = Console.ReadLine();
            Console.WriteLine("Enter College Name : ");
            college = Console.ReadLine();
            Console.WriteLine("Enter Gender : ");
            gender = Convert.ToChar(Console.ReadLine());
            Console.WriteLine("Enter DOB : ");
            dob = Convert.ToDateTime(Console.ReadLine());
            Console.WriteLine("Enter Height : ");
            height = Convert.ToDouble(Console.ReadLine());
            Console.WriteLine();
            Console.WriteLine("Id is : "+ID);
            Console.WriteLine("Name is : "+name);
            Console.WriteLine("College is : "+college);
            Console.WriteLine("Gender is : "+gender);
            Console.WriteLine("Dob is : "+dob);
            Console.WriteLine("Height is : "+height);

        }
    }
}
