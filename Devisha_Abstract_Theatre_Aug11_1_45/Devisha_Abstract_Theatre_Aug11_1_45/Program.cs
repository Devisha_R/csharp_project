﻿using System;

namespace Devisha_Abstract_Theatre_Aug11_1_45
{
    class Program
    {
        abstract class Theatre {

            public abstract void capacity();

            public abstract void snacks();
           
        }
        class Movie : Theatre
        {
            int size;
            char snack;
            string name;
            public Movie(int si,
            char sn,string n)
            {
                size = si;
                snack = sn;
                name = n;
            }
            public void show_name()
            {
                Console.WriteLine("Name : " + name);
            }
            public override void capacity()
            {
                Console.WriteLine("Number of Seats : "+size);
            }
            public override void snacks()
            {
                Console.WriteLine("Snacks  : " + snack);
            }




        }
        static void Main(string[] args)
        {
            Movie m = new Movie(300, 'Y',"The Show");
            m.show_name();
            m.capacity();
            m.snacks();

        }
    }
}
