﻿using System;
using System.Collections;

namespace Devisha_IEnumExample_aug12
{
    class Program
    {
        
        static void Main(string[] args)
        {
            int [] i = new int[] {1,2,3,4 };
            int [] id = new int[i.Length];
            i.CopyTo(id, 0);
            Console.WriteLine("Old array :");
            foreach(int n in i)
            {
                Console.WriteLine(n);
            }
            Console.WriteLine("New array :");
            foreach (int n in id)
            {
                Console.WriteLine(n);
            }
            i.Clone();
            /*ArrayList al = new ArrayList();
            al.Add(678);
            al.Add("Devisha");
            al.Add(56.89);
            string [] al1 = new string[5];
            al1[0] = "1";
            al1[1] = "2";
            
            Console.WriteLine("Old arraylist :");
            Console.WriteLine(al);
            Console.WriteLine("New array :");
            Console.WriteLine(al1);*/
        }
    }
}
