﻿using System;

namespace Devisha_Delegate2_Aug15
{
   
    class Program
    {
        public delegate void ShowMsg(string str);
        public void Show(string str)
        {
            Console.WriteLine("Message is : " + str);
        }
        static void Main(string[] args)
        {
            Program obj = new Program();
            ShowMsg sm = new ShowMsg(obj.Show);
            sm("Hello Devisha");
        }
    }
}
