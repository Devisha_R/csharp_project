﻿using System;

namespace Devisha_Delegate3_Aug15
{
    class Program
    {
        public delegate void Add(int num1, int num2);
        
        public delegate void Sub(int num1, int num2);

        public void AddNum(int num1,int num2)
        {
            Console.WriteLine("Sum is : " + (num1 + num2));
        }
        public void SubNum(int num1,int num2)
        {
            Console.WriteLine("Difference is : " + (num1-num2));
        }
        static void Main(string[] args)
        {
            Program p1 = new Program();
            Add a = new Add(p1.AddNum);
            Sub s = new Sub(p1.SubNum);

            a(45,76);
            s(76,45);
        }
    }
}
