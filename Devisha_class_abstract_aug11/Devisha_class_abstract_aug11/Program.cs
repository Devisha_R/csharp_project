﻿using System;

namespace Devisha_class_abstract_aug11
{
    class Program
    {
        abstract class Class1
        {
            public abstract int total();
        }
        class Student : Class1
        {
            public int f, m;
            public Student(int fe,int me)
            {
                f = fe;
                m = me;

            }
            public override int total()
            {
                return (f + m);
            }

        }
        static void Main(string[] args)
        {
            Student s1 = new Student(15, 20);
            int t=s1.total();
            Console.WriteLine("Total is : " + t);
        }
    }
}
