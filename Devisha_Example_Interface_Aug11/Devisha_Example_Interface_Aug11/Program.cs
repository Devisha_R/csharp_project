﻿using System;

namespace Devisha_Example_Interface_Aug11
{
    class Program : IMulti,IDiv,IAdd_Sub
        
    {
        int num1 = 9;
        int num2 = 2;
        static void Main(string[] args)
        {
            Program p1 = new Program();
            p1.multi();
            p1.Div();
            p1.Add();
            p1.Sub();
        }

        
        public void multi()
        {
            Console.WriteLine("Mult is : "+(num1*num2));
        }

        public void Div()
        {
            Console.WriteLine("Div is : "+ (num1 / num2));
        }

        public void Add()
        {
            Console.WriteLine("addition is : "+ (num1 +num2));

        }

        public void Sub()
        {
            Console.WriteLine("Sub is : " + (num1 - num2));
        }
    }
}
