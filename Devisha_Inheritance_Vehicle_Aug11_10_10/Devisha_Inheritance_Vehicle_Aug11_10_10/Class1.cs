﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Devisha_Inheritance_Vehicle_Aug11_10_10
{
  public  class Class1
    {
        public int no_of_student;
        public int standard;
        public string mentor;
        public void show_Strength()
        {
            Console.WriteLine("No. of student is : " + no_of_student);
        }
        public void show_Standard()
        {
            Console.WriteLine("Standard is : " + standard);
        }
        public void show_Mentor()
        {
            Console.WriteLine("mentor is : " + mentor);
        }



    }
    class Student : Class1
    {
        public string name;
        public int age;
        public void show_Name_Age()
        {
            Console.WriteLine("Name is : " + name+" age is : "+age);
        }


    }
   public class UseStudent
    {
        public static void Main(string[] args)
        {
            Student s1 = new Student();
            Console.WriteLine("Enter Name,age,mentor,standard,no. of student : ");
            s1.name = Console.ReadLine();
            s1.age = Convert.ToInt32(Console.ReadLine());
            s1.mentor = Console.ReadLine();
            s1.standard = Convert.ToInt32(Console.ReadLine());
            s1.no_of_student= Convert.ToInt32(Console.ReadLine());

            s1.show_Name_Age();
            s1.show_Mentor();
            s1.show_Standard();
            s1.show_Strength();
        }
    }
}
