﻿using System;

namespace Devisha_Inheritance_Vehicle_Aug11_10_10
{
     class Vehicle
    {
        public string fuel_type;
        public string colour;
        public string company;
        //public int tank_capacity;
        
        public void showColour()
        {
            Console.WriteLine("Colour is : " + colour);
        }
        public void showFuelType()
        {
            Console.WriteLine("Fuel Type is : " + fuel_type);
        }

    }
     class Car : Vehicle
    {
        public string car_type;
        public void showCarType()
        {
            Console.WriteLine("Car Type : " + car_type);
        }
        public void showComapny()
        {
            Console.WriteLine("Company is : " + company);
        }
    }

    class Program
    {
        static void Main(string[] args)
        {

            Car car_obj = new Car();
            // Car car_obj2 = new Car();
            Console.WriteLine("Enter fuel type , colour , company and car Type : ");

            car_obj.fuel_type = Console.ReadLine();
            car_obj.colour = Console.ReadLine();
            car_obj.company = Console.ReadLine();
            car_obj.car_type = Console.ReadLine();
            car_obj.showFuelType();
            car_obj.showColour();
            car_obj.showCarType();
            car_obj.showComapny();
            //Console.WriteLine("Hello World!");
        }
    }
}
