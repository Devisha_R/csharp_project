﻿using System;

namespace Devisha_Abstract_Vehicle_Aug11
{

    class Program
    {
        abstract class Vehicle
        {
            
            public abstract void showName();
            

             
        }
        class Car : Vehicle
        {
            string n;
            public Car(string name)
            {
                n = name;
            }
            public override void showName()
            {
                Console.WriteLine("Company is : " + n);
            }


        }
        static void Main(string[] args)
        {
            Car c1 = new Car("Ford");
            c1.showName();
            Console.WriteLine("");
        }
    }
}
