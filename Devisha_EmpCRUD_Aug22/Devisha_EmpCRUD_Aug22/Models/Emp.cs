﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Devisha_EmpCRUD_Aug22.Models
{
    public class Emp
    {
        public Guid empid { get; set; }
        public string name { get; set; }
        public DateTime doj { get; set; }
    }
}
