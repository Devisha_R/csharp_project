﻿using Devisha_EmpCRUD_Aug22.Models;
using Microsoft.AspNetCore.Mvc;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Devisha_EmpCRUD_Aug22.Controllers
{
    public class EmpController : Controller
    {
        private static List<Emp> emplist = new List<Emp>(){
            new Emp{empid=Guid.NewGuid(),name="Devisha",doj=new DateTime()},
            new Emp{empid=Guid.NewGuid(),name="Joy",doj=new DateTime()}

        };
        public IActionResult Index()
        {
            return View();
        }
        public ActionResult Details()
        {
            return View(emplist);
        }
        public ActionResult AddEmp()
        {
            return View();
        }
        [HttpPost]
        public ActionResult AddEmp(Emp input)
        {
            emplist.Add(new Emp { empid = Guid.NewGuid(), name = input.name, doj = input.doj });
                return View();
        }
        [HttpPost]
        public ActionResult Delete(Guid id)
        {
            emplist.Remove(emplist.SingleOrDefault(o=>o.empid==id));
            return RedirectToAction("Details");
        }
        [HttpGet]
        public ActionResult Edit(Guid id)
        {
            return View(emplist.Single(o => o.empid == id));
        }
        [HttpPost]
        public ActionResult Edit(Emp e)
        {
            if (ModelState.IsValid)
            {
                var em = emplist.Where(x => x.empid == e.empid).SingleOrDefault();
                emplist.Remove(em);
                emplist.Add(e);
                return RedirectToAction("Details");
                ViewBag.message = "Edited Successfully";
            };
            return View();

        }

    }
}
