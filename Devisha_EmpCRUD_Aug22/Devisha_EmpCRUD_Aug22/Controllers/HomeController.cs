﻿using Devisha_EmpCRUD_Aug22.Models;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Logging;
using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Threading.Tasks;

namespace Devisha_EmpCRUD_Aug22.Controllers
{
    public class HomeController : Controller
    {
        //private readonly ILogger<HomeController> _logger;

        //public HomeController(ILogger<HomeController> logger)
        //{
        //    _logger = logger;
        //}
        //private static List<Emp> emplist = new List<Emp>(){
        //    new Emp{empid=Guid.NewGuid(),name="Devisha",doj=new DateTime()},
        //    new Emp{empid=Guid.NewGuid(),name="Joy",doj=new DateTime()}

        //};
        public IActionResult Index()
        {
            return View();
        }
        public IActionResult Details()
        {
            return RedirectToAction("Details", "EmpController");
        }
        public IActionResult Privacy()
        {
            return View();
        }

        [ResponseCache(Duration = 0, Location = ResponseCacheLocation.None, NoStore = true)]
        public IActionResult Error()
        {
            return View(new ErrorViewModel { RequestId = Activity.Current?.Id ?? HttpContext.TraceIdentifier });
        }
    }
}
