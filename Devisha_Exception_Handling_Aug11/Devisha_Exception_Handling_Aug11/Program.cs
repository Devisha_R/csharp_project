﻿using System;

namespace Devisha_Exception_Handling_Aug11
{
    class Program
    {
        
        static void Main(string[] args)
        {
            int[] arr = new int[4] { 1, 2, 3, 4 };
            int num1 = 2;
            int num2 = 0;
            try
            {
                Console.WriteLine(num1 / num2);

                Console.WriteLine(arr[6]);
                


            }
            catch(IndexOutOfRangeException ior)
            {
                Console.WriteLine("Wrong index ");
                Console.WriteLine(ior.Message);
            }
            catch(Exception e)
            {
                Console.WriteLine(e.Message);
            }
            {

            }
           

        }
    }
}
