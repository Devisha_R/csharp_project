﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Devisha_Student_Dashboard_Aug21.Models
{
    public class Student
    {
        public Guid stud_id { get; set; }
        public string stud_name { get; set; }
        public string role { get; set; }
        public DateTime doj { get; set; }
        public DateTime dob { get; set; }
        
    }
}
