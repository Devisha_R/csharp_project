﻿using System;

namespace Devisha_Abstract_Classs_Aug11
{
    abstract public class Shape
    {
        abstract public void area();
    }
    public class Triangle: Shape
    {
        int side = 0;
        public Triangle(int i)
        {
            side = i;

        }
        public override void area()
        {
            Console.WriteLine("Area is : " + (side * side));

        }
    }
    class Program
    {
        static void Main(string[] args)
        {
            Triangle t1 = new Triangle(7);
            t1.area();
            Console.WriteLine("Hello World!");
        }
    }
}
