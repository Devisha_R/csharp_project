﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Devisha_WebExample1_Aug18.Models
{
    public class Student
    {
        public Guid StudentId { get; set; }
        public string StudentName { get; set; }
        public char Section { get; set; }
        public char Grade { get; set; }
        public Guid Id { get; set; }
    }
}
