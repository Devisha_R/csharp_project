﻿using Devisha_WebExample1_Aug18.Models;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Logging;
using System;
using System.Collections;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Threading.Tasks;

namespace Devisha_WebExample1_Aug18.Controllers
{
    public class HomeController : Controller
    {
        private readonly ILogger<HomeController> _logger;

        private static List<Student> al = new List<Student>
            {
                new Student(){ StudentId = Guid.NewGuid(), StudentName = "Devisha", Section = 'A', Grade = 'A'},
                new Student(){ StudentId = Guid.NewGuid(), StudentName = "Riya",  Section = 'A', Grade = 'B'},
                new Student(){ StudentId = Guid.NewGuid(), StudentName = "Heli",  Section = 'A', Grade = 'C'},
                new Student(){ StudentId = Guid.NewGuid(), StudentName = "Shree", Section = 'A', Grade = 'D'},
                new Student(){ StudentId = Guid.NewGuid(), StudentName = "Akhil", Section = 'A',Grade = 'E'}
            };
        
        public HomeController(ILogger<HomeController> logger)
        {
            _logger = logger;
        }

        public IActionResult Index()
        {
            ViewData["abc"] = "This is View Data";
            ViewBag.someexample = "This is View Bag";
            return View();

        }
        public ViewResult Details()
        {
            Student student = new Student();
            {
                student.StudentId = Guid.NewGuid();
                student.StudentName = "Devisha";
                student.Section = 'B';
                student.Grade = 'A';
                //Guid gobj = new Guid();
                Guid gid = Guid.NewGuid();
                student.Id = gid;

            };
            ViewData["Student"] = student;
           
            return View() ;
        }
        public ViewResult DetailsList()
        {
            
            
            ViewData["studList"] = al;
            return View();
        }
        public IActionResult AddStudent()
        {
            return View();
        }
        [HttpPost]
        public IActionResult AddStudent(Student input)
        {
           al.Add(new Student { StudentId = Guid.NewGuid(), StudentName = input.StudentName });
            return RedirectToAction("DetailsList");
        }
        public IActionResult Privacy()
        {
            return View();
        }

        [ResponseCache(Duration = 0, Location = ResponseCacheLocation.None, NoStore = true)]
        public IActionResult Error()
        {
            return View(new ErrorViewModel { RequestId = Activity.Current?.Id ?? HttpContext.TraceIdentifier });
        }
    }
}
