﻿using System;

namespace Devisha_Grade_Aug10
{

    class Program
    {
        static double cal_per(int mark1,int mark2,int  mark3,int  mark4,int mark5, int mark6)
        {
            int sum = mark1 + mark2 + mark3 + mark4 + mark5 + mark6;
            double per = sum / 6.0;
            return per;
        }
        
        static void Main(string[] args)
        {
            int mark1, mark2, mark3, mark4, mark5, mark6;
            Console.WriteLine("Enter First Subject Marks : ");
            mark1 = Convert.ToInt32(Console.ReadLine());
            Console.WriteLine("Enter Second Subject Marks : ");
            mark2 = Convert.ToInt32(Console.ReadLine());
            Console.WriteLine("Enter Third Subject Marks : ");
            mark3 = Convert.ToInt32(Console.ReadLine());
            Console.WriteLine("Enter Fourth Subject Marks : ");
            mark4 = Convert.ToInt32(Console.ReadLine());
            Console.WriteLine("Enter Fifth Subject Marks : ");
            mark5 = Convert.ToInt32(Console.ReadLine());
            Console.WriteLine("Enter Sixth Subject Marks : ");
            mark6 = Convert.ToInt32(Console.ReadLine());

           double per= cal_per(mark1, mark2, mark3, mark4, mark5, mark6);
            if (per > 90.0)
            {
                Console.WriteLine("A");
            }
            else if ((per > 80.0) && (per <= 90.0))
            {
                Console.WriteLine("B");
            }
            

        }
    }
}
