﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;

namespace Devisha_DBCRUD_Aug22.Models
{
    public partial class EmployeeTable1
    {   [Required]
        public int Empid { get; set; }
        [Required]
        public string Empname { get; set; }
    }
}
