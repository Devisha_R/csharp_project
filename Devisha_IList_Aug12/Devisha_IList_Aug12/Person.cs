﻿using System;
using System.Collections;

namespace Devisha_IList_Aug12
{
    class Person
    {
        string name;
        int id;
        public Person(int id_,string name_)
        {
            id = id_;
            name = name_;
        }
        
        public  int getId()
        {
            return id;

        }
        public String getString()
        {
            return name;

        }


        static void Main(string[] args)
        {
            ArrayList arr = new ArrayList();
            Person p = new Person(1, "Devisha");
            Person p1 = new Person(2, "Rahangdale");
            arr.Add(p);
            arr.Add(p1);
            foreach(Person p_ in arr)
            {
                Console.WriteLine(p_);
            }
           // Console.WriteLine();
            //Console.WriteLine("Hello World!");
        }
    }
}
