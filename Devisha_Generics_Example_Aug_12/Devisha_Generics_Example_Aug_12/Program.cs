﻿using System;

namespace Devisha_Generics_Example_Aug_12
{
    class Program
    {
        public class SomeClass
        {
            public void GenericMethod <T>(T Param1,T Param2)
            {
                
                Console.WriteLine($"Parameter 1 :{Param1} : Parameter 2 : {Param2}");

            }
        }
        static void Main(string[] args)
        {
            SomeClass co = new SomeClass();
            co.GenericMethod(1, 2);
            //private <T> data;
            //data = "Hi";
            //data = 2;
            //data = 'C';
            //public <T>[] arr;

            Console.WriteLine("Generics Method Example");
            SomeClass s = new SomeClass();
            s.GenericMethod<int>(10, 20);
            s.GenericMethod (10.5, 20.5);
            s.GenericMethod("Hi", "Devisha");
            Console.ReadLine();

            


        }
    }
}
