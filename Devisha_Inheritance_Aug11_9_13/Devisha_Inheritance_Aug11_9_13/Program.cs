﻿using System;

namespace Devisha_Inheritance_Aug11_9_13
{
    class Shape
    {
        public double width;
        public double height;
       public void showDim()
        {
            Console.WriteLine("Width is " + width + " and Height is " + height);
        }
    }
    class Triangle : Shape
    {
        public string style;
        public double area()
        {
            return ((width * height) / 2);
        }
        public void showStyle()
        {
            Console.WriteLine("Triangle is " + style);
        }
    }
    class Program
    {
        static void Main(string[] args)
        {
            Triangle t1 = new Triangle();
            Triangle t2 = new Triangle();
            Console.WriteLine("Enter Width and height : ");
            t1.width = Convert.ToDouble(Console.ReadLine());
            t1.height = Convert.ToDouble(Console.ReadLine());
            t1.showDim();
            double a1=t1.area();
            Console.WriteLine("area is : " + a1);
            Console.WriteLine("Enter style :");
            t1.style = Console.ReadLine();
            t1.showStyle();
            Console.WriteLine("Enter Width and height : ");
            t2.width = Convert.ToDouble(Console.ReadLine());
            t2.height = Convert.ToDouble(Console.ReadLine());
            t2.showDim();
            double a2 = t2.area();
            Console.WriteLine("area is : " + a2);
            Console.WriteLine("Enter style :");
            t2.style = Console.ReadLine();
            t2.showStyle();


            //Console.WriteLine("Hello World!");
        }
    }

}
