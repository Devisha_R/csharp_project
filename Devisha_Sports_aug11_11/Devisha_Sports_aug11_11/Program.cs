﻿using System;

namespace Devisha_Sports_aug11_11
{
    class Sport
    {
        public int no_of_player;
        public string board;
         public void show_No_of_Player()
        {
            Console.WriteLine("Number of player is : " + no_of_player);
        }
        public void show_Board()
        {
            Console.WriteLine("Board is : " + board);
        }


    }
    class Cricket : Sport
    {
        public double ground_size;
            public void show_ground_size()
        {
            Console.WriteLine("Ground size is : "+ground_size);
        }


    }
    
    class Program
    {
        static void Main(string[] args)
        {
            Console.WriteLine("Enter Number of player,board,ground size : ");
            Cricket c1 = new Cricket();
            c1.no_of_player = Convert.ToInt32(Console.ReadLine());
            c1.board = Console.ReadLine();
            c1.ground_size= Convert.ToDouble(Console.ReadLine());
            c1.show_No_of_Player();
            c1.show_Board();
            c1.show_ground_size();
        }
    }
}
