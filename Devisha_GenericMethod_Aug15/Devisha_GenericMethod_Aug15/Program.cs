﻿using System;

namespace Devisha_GenericMethod_Aug15
{
    class Gen
    {

        public void Display<T>(T dis)
        {
            Console.WriteLine("Hi from Genric Method , input given is : " + dis);

        }
    }
    class Program
    {
        static void Main(string[] args)
        {
            Gen obj1 = new Gen();
            Gen obj2 = new Gen();
            Gen obj3 = new Gen();
            obj1.Display<char>('D');
            obj2.Display<string>("Devisha");
            obj3.Display<int>(345);



            
        }
    }
}