﻿using System;

namespace Devisha_Sub_Aug9_3_19
{
    class Program
    {
        static void Main(string[] args)
        {
            //Declaring two Variables
            int num1, num2;
            
            Console.WriteLine("Enter First Num :");
            //Taking Input in Variable num1
            num1 = Convert.ToInt32(Console.ReadLine());
            Console.WriteLine("Enter Second Num : ");
            //Taking Input in variable num2
            num2 = Convert.ToInt32(Console.ReadLine());
            //Printing the difference
            Console.WriteLine("Difference is " + (num1 - num2));

        }
    }
}
