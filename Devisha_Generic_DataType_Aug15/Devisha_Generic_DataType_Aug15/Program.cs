﻿using System;

namespace Devisha_Generic_DataType_Aug15
{
    class Gen<T>
    {
        public T val;
    }
    class Program

    {
        static void Main(string[] args)
        {
            Gen<int> obj1 = new Gen<int>();
            obj1.val = 90;
            Gen<string> obj2 = new Gen<string>();
            obj2.val = "Hi Devisha";
            Console.WriteLine(obj1.val);
            Console.WriteLine(obj2.val);
        }
    }
}
