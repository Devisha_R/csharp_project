﻿using System;

namespace Devisha_delegate_Aug15
{
    class Program
    {
        public delegate void MultiDelegate(int num1,int num2);
        void Multi(int num1, int num2)
        {
            Console.WriteLine(num1 * num2);
        }

        static void Main(string[] args)
        {
            Program pobj = new Program();
            MultiDelegate m = new MultiDelegate(pobj.Multi);
            m(50, 10);
        }
    }
}
