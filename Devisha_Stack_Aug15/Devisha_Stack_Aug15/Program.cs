﻿using System;
using System.Collections;

namespace Devisha_Stack_Aug15
{
    class Program
    {
        static void Main(string[] args)
        {
            Stack s = new Stack();
            s.Push("Hi");
            s.Push("Hello");

            Console.WriteLine("Stack : ");
            foreach (object o in s)
            {
                Console.WriteLine(o);
            }
            Console.WriteLine(s.Count);
            object obj1=s.Pop();
            Console.WriteLine("Stack after popping : ");
            foreach (object o in s)
            {
                Console.WriteLine(o);
            }

        }
    }
}
