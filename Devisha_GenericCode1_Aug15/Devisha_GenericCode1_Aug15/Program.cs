﻿using System;

namespace Devisha_GenericCode1_Aug15
{
    class GenClass<T>
    {
        public GenClass(T str)
        {
            Console.WriteLine("Hello from Generic class \nValue entered is :" + str);

        }
    }
    class Program 
    {
        
        static void Main(string[] args)
        {
            GenClass<int> pobj = new GenClass<int>(567);
            GenClass<double> pobj1 = new GenClass<double>(84.6);
            GenClass<string> pobj2 = new GenClass<string> ("This is string type");
        }
    }
}
