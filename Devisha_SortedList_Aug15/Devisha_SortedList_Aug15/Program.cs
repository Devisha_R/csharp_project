﻿using System;
using System.Collections;

namespace Devisha_SortedList_Aug15
{
    class Program
    {
        static void Main(string[] args)
        {
            SortedList sl = new SortedList();
            
            sl.Add(1, "Devisha");
            sl.Add(2, "shree");
            sl.Add(3, "Meera");

            foreach (DictionaryEntry d in sl)
            {
                Console.WriteLine(d.Key + " " + d.Value);
            }
            Console.WriteLine(sl.Count);
        }
    }
}
